"""Main Module of the program"""
import os
import re
import mpv
from simple_term_menu import TerminalMenu

class MainClass:
    """Master class of the program"""
    def __init__(self, path, media, results, select, full_path, file_name):
        self.path = path
        self.media = media
        self.results = results
        self.select = select
        self.full_path = full_path
        self.file_name= file_name

    def searching(self):
        """Search media via info"""
        self.path = input("Type the path you want to search. ")
        self.media = os.listdir(self.path)

    def filtering(self):
        """Filter the media by file name"""
        name=input("Type the name you want to search. ")
        r = re.compile(name)
        self.results = list(filter(r.match, self.media))
        print(self.results)

    def selecting(self):
        """Selecting What media to play"""
        terminal_menu = TerminalMenu(self.results)
        self.select=terminal_menu.show()
        self.file_name=self.results[self.select]
        self.full_path=self.path+self.file_name

    def playing(self):
        """Playing selected media"""
        player = mpv.MPV(osc=True)
        player.play(self.full_path)
        player.wait_for_playback()

def main():
    """Main function of the program"""
    o=MainClass(path='$HOME', media=None, results=None, select=None, full_path=None, file_name=None)
    o.searching()
    o.filtering()
    o.selecting()
    o.playing()

if __name__=="__main__":
    main()
