"""File chooser"""
from os import listdir
from simple_term_menu import TerminalMenu

class FileChooser:
    """Find Directorys"""
    def __init__ (self, starting_location, point_index, point):
        self.starting_location= starting_location
        self.point_index = point_index
        self.point = point

    def show_directory(self):
        """Navige file system"""
        dir_list = listdir(self.starting_location)
        terminal_menu=TerminalMenu(self.starting_location)
        self.point_index=terminal_menu.show()
        self.point=dir_list[self.point_index]
        print(self.point)

def main():
    fchooser=FileChooser(starting_location='/home/musicfan', point_index=None, point=None)
    fchooser.show_directory()

if __name__=="__main__":
    main()
